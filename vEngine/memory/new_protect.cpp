//-----------------------------------------------------------------------------
// File: new_protect
// Auth: Lyp
// Date: 2004-2-26	
// Last: 2004-2-26
//-----------------------------------------------------------------------------
#include "..\stdafx.h"
#include "new_protect.h"

namespace vEngine {
//-----------------------------------------------------------------------------
// The results of the new testing, if discovery fails, the user is prompted to choose whether to exit / Retry / Resume
//-----------------------------------------------------------------------------
INT NewHandle( size_t size )
{
	// Prompt the user, allowing users to handle
	TObjRef<Debug>()->ErrMsg(_T("MALLOC(or NEW) for %lu bytes failed\r\n"), size);
	return 1;
}

}