//-----------------------------------------------------------------------------
// File: new_protect
// Auth: Lyp
// Date: 2004-2-26	
// Last: 2004-2-26
//-----------------------------------------------------------------------------
#pragma once
namespace vEngine {
//-----------------------------------------------------------------------------
/* Design Concept
? 1. In order to avoid frequent throughout the program that checks the return value of new, here for a unified treatment
? 2. When memory allocation fails, first try to free some memory from the memory pool is not being used
? 3. The user is prompted to choose whether to exit / Retry / Resume
*/
//-----------------------------------------------------------------------------
INT NewHandle( size_t size );
}
