//-----------------------------------------------------------------------------
// File: mem_pool
// Desc: game tool mem pool 2.0
// Auth: Lyp
// Date: 2009-1-8	
// Last: 2009-3-18
//-----------------------------------------------------------------------------
#pragma once
namespace vEngine {
//-----------------------------------------------------------------------------
// Memory pools (Note: The size of the memory pool represents: the free memory in the memory pool size)
// lookaside list
// Initialization:
// 1 External memory pool set maximum allowable size
// 2 Memory pool without any pre-allocated memory
//
// When the external application memory, in order to determine:
// 1 If there is a correspondingly sized memory pool of free blocks, the use of free blocks return
// 2 When there is no free blocks, and apply directly in memory before the memory is added tagMemItem, then returns
//
// When the external memory is released, in order to determine:
// 1 If the release of the memory block size> The maximum size of the memory pool, released directly into system memory
// 2 If the current size of the memory pool + release memory block size <maximum size of the memory pool, directly into the memory pool
// 3 Garbage collection: Recheck step 2 above after garbage collection, if not through, proceed to step 4
// 4 Otherwise released directly into system memory
//-----------------------------------------------------------------------------
class VENGINE_API MemPool
{
public:
	__forceinline LPVOID Alloc(DWORD dwBytes);
	__forceinline VOID  Free(LPVOID pMem);
	__forceinline DWORD GetSize() { return m_dwCurrentSize; }

	MemPool(DWORD dwMaxPoolSize=16*1024*1024);
	~MemPool();

private:
	struct tagNode	// Memory block header description
	{
		tagNode*	pNext;
		tagNode*	pPrev;
		INT			nIndex;
		DWORD		dwSize;
		DWORD		dwUseTime;
		DWORD		pMem[1];	// The actual memory space
	};

	struct
	{
		tagNode*	pFirst;
		tagNode*	pLast;
	} m_Pool[16];

	DWORD m_dwMaxSize;		// External setting maximum allowable free memory
	DWORD m_dwCurrentSize;	// Total memory pool of free memory

	// Garbage Collection
	__forceinline VOID GarbageCollect(DWORD dwExpectSize, DWORD dwUseTime);
	// Returns the size that best matches
	__forceinline INT SizeToIndex(DWORD dwSize, DWORD& dwRealSize);
};


//-----------------------------------------------------------------------------
// Distribution
//-----------------------------------------------------------------------------
LPVOID MemPool::Alloc(DWORD dwBytes)
{
	DWORD dwRealSize = 0;
	INT nIndex = SizeToIndex(dwBytes, dwRealSize);
	if( GT_INVALID != nIndex )
	{
		if( m_Pool[nIndex].pFirst )	// There are pool, allocated from the pool
		{
			tagNode* pNode = m_Pool[nIndex].pFirst;
			m_Pool[nIndex].pFirst = m_Pool[nIndex].pFirst->pNext;
			if( m_Pool[nIndex].pFirst )
				m_Pool[nIndex].pFirst->pPrev = NULL;
			else
				m_Pool[nIndex].pLast = NULL;
			m_dwCurrentSize -= dwRealSize;
			++pNode->dwUseTime;
			return pNode->pMem;	
		}
	}

	tagNode* pNode = (tagNode*)malloc(dwRealSize + sizeof(tagNode) - sizeof(DWORD));
	if( !pNode )
		return NULL;

	pNode->nIndex = nIndex;
	pNode->dwSize = dwRealSize;
	pNode->pNext = NULL;
	pNode->pPrev = NULL;
	pNode->dwUseTime = 0;
	return pNode->pMem;	// Actual memory allocated from
}


//-----------------------------------------------------------------------------
// Freed
//-----------------------------------------------------------------------------
VOID MemPool::Free(LPVOID pMem)
{
	tagNode* pNode = (tagNode*)(((LPBYTE)pMem) - sizeof(tagNode) + sizeof(DWORD));
	if( GT_INVALID != pNode->nIndex )
	{
		if( pNode->dwSize + m_dwCurrentSize > m_dwMaxSize && pNode->dwUseTime > 0 )
			GarbageCollect(pNode->dwSize*2, pNode->dwUseTime);	// Garbage Collection

		if( pNode->dwSize + m_dwCurrentSize <= m_dwMaxSize ) // Memory pool can accommodate
		{
			pNode->pPrev = NULL;
			pNode->pNext = m_Pool[pNode->nIndex].pFirst;
			if( m_Pool[pNode->nIndex].pFirst )
				m_Pool[pNode->nIndex].pFirst->pPrev = pNode;
			else
				m_Pool[pNode->nIndex].pLast = pNode;

			m_Pool[pNode->nIndex].pFirst = pNode;
			m_dwCurrentSize += pNode->dwSize;
			return;
		}
	}

	free(pNode);
}


//-----------------------------------------------------------------------------
// Garbage Collection
//-----------------------------------------------------------------------------
VOID MemPool::GarbageCollect(DWORD dwExpectSize, DWORD dwUseTime)
{
	DWORD dwFreeSize = 0;
	for(INT n=15; n>=0; --n)	// From the beginning of the largest recycling
	{
		if( !m_Pool[n].pFirst )
			continue;

		tagNode* pNode = m_Pool[n].pLast; // Beginning from the last few releases, because the back of the Node frequency of use
		while( pNode )
		{
			tagNode* pTempNode = pNode;
			pNode = pNode->pPrev;
			if( pTempNode->dwUseTime < dwUseTime )	// This node release
			{
				if( pNode )
					pNode->pNext = pTempNode->pNext;
				if( pTempNode->pNext )
					pTempNode->pNext->pPrev = pNode;
				if( m_Pool[n].pLast == pTempNode )
					m_Pool[n].pLast = pNode;
				if( m_Pool[n].pFirst == pTempNode )
					m_Pool[n].pFirst = pTempNode->pNext;

				m_dwCurrentSize -= pTempNode->dwSize;
				dwFreeSize += pTempNode->dwSize;
				free(pTempNode);
			}

			if( dwFreeSize >= dwExpectSize )
				return;
		}
	}
}


//-----------------------------------------------------------------------------
// Returns the size that best matches
//-----------------------------------------------------------------------------
INT MemPool::SizeToIndex(DWORD dwSize, DWORD& dwRealSize)
{
	if( dwSize<=32 )		{ dwRealSize = 32;			return 0; }
	if( dwSize<=64 )		{ dwRealSize = 64;			return 1; }
	if( dwSize<=128 )		{ dwRealSize = 128;			return 2; }
	if( dwSize<=256 )		{ dwRealSize = 256;			return 3; }
	if( dwSize<=512 )		{ dwRealSize = 512;			return 4; }
	if( dwSize<=1024 )		{ dwRealSize = 1024;		return 5; }
	if( dwSize<=2*1024 )	{ dwRealSize = 2*1024;		return 6; }
	if( dwSize<=4*1024 )	{ dwRealSize = 4*1024;		return 7; }
	if( dwSize<=8*1024 )	{ dwRealSize = 8*1024;		return 8; }
	if( dwSize<=16*1024 )	{ dwRealSize = 16*1024;		return 9; }
	if( dwSize<=32*1024 )	{ dwRealSize = 32*1024;		return 10; }
	if( dwSize<=64*1024 )	{ dwRealSize = 64*1024;		return 11; }
	if( dwSize<=128*1024 )	{ dwRealSize = 128*1024;	return 12; }
	if( dwSize<=256*1024 )	{ dwRealSize = 256*1024;	return 13; }
	if( dwSize<=512*1024 )	{ dwRealSize = 512*1024;	return 14; }
	if( dwSize<=1024*1024 )	{ dwRealSize = 1024*1024;	return 15; }
	dwRealSize = dwSize;
	return GT_INVALID;
}
} // namespace vEngine {
