//-----------------------------------------------------------------------------
//!\file mem_map.h
//!\author Lyp
//!
//!\date 2004-06-17
//! last 2009-03-05
//!
//!\brief �ڴ����
//-----------------------------------------------------------------------------
#pragma once

#ifdef _MEM_TRACE_

#undef new
#undef malloc
#undef calloc
#undef realloc
#undef free

#include <string>
namespace vEngine {
//-----------------------------------------------------------------------------
//!\Brief memory analysis
//! Memory leak when you need to find the call stack, you can set a breakpoint in the Alloc function, and then start the program,
//!	When the program is broken down, the m_dwBreakAddress modified to address memory leaks, and then cancel the breakpoint continue
//! Run the program. When the memory allocation set address occurs, Alloc will automatically break down
//-----------------------------------------------------------------------------

//Memory leak analysis parameters, only to report later than this time a memory leak in milliseconds
#define  START_MEM_CHECK_TICK	180 * 1000
class VENGINE_API MemMap
{
public:
	LPVOID Alloc(LPCSTR szFile, INT nLine, size_t uiSize, BOOL bArray);
	VOID Free(LPVOID pAddress, BOOL bArray);
	VOID WriteFile();
	FILE *CreateMemFile();
	LPCTSTR Env();
	VOID ShowMemUseSorted();

	MemMap();
	~MemMap();

private:
	struct tagMemNode
	{
		LPVOID	pAddress;		// Address assignment
		LPCSTR	szFile;			// File name
		INT		nLine;			// Line number
		size_t	uiSize;			// Allocation size
		size_t  uiTickCount;    // Time allocated
		bool	bArray;			// Whether array request
		bool	bArrayError;	// Array release error
		bool	bBoundError;	// Cross-border operations
		bool	bReserved;		// To align reserved
	};

	struct tagNodeStat
	{
		std::string	nodeIdentify;	//+ According to the file number of lines composed of blocks of memory logo
		size_t	uiAllocCount;	//The frequency distribution
		size_t	uiFreeCount;	//Release times
		size_t	uiRemainSize;	//Currently reserved memory space
	};

	VOLATILE BOOL					m_bTerminate;
	TSafeMap<LPVOID, tagMemNode*>	m_mapMem;
	TSafeList<tagMemNode*>			m_listArrayErrorNode;
	TSafeList<tagMemNode*>			m_listBoundErrorNode;

	DWORD							m_dwStartTick; //Class initialization time to time

	DWORD m_dwBreakAddress;
};

} // namespace vEngine {

VOID VENGINE_API WriteMem();
VOID VENGINE_API ShowMemUseSorted();
LPCTSTR VENGINE_API EnvString();

LPVOID VENGINE_API AllocateMemory(size_t uiSize, LPCSTR szFile, INT nLine, BOOL bArray, LPVOID pAddress=NULL); 
VOID VENGINE_API DeallocateMemory(LPVOID pAddress, BOOL bArray);

inline LPVOID operator new(size_t uiSize, LPCSTR szFile, INT nLine) { return AllocateMemory(uiSize, szFile, nLine, FALSE); }
inline LPVOID operator new[](size_t uiSize, LPCSTR szFile, INT nLine) { return AllocateMemory(uiSize, szFile, nLine, TRUE); }
inline VOID operator delete(LPVOID pAddress) { DeallocateMemory(pAddress, FALSE); }
inline VOID operator delete[](LPVOID pAddress) { DeallocateMemory(pAddress, TRUE); }

// These two routines should never get called, unless an error occures during the 
// allocation process.  These need to be defined to make Visual C++ happy.
inline VOID operator delete(LPVOID pAddress, LPCSTR szFile, INT nLine) { szFile;nLine;DeallocateMemory(pAddress, FALSE); }
inline VOID operator delete[](LPVOID pAddress, LPCSTR szFile, INT nLine) { szFile;nLine;DeallocateMemory(pAddress, TRUE); }

#define new							new(__FILE__, __LINE__)
#define malloc(uiSize)				AllocateMemory(uiSize, __FILE__, __LINE__,  FALSE)
#define calloc(uiNum, uiSize)		AllocateMemory(((uiSize)*(uiNum)), __FILE__, __LINE__, FALSE)
#define realloc(pAddress, uiSize)	AllocateMemory(uiSize, __FILE__, __LINE__, FALSE, pAddress)
#define free(pAddress)				DeallocateMemory(pAddress, FALSE)


#endif // #ifdef _MEM_TRACE_
