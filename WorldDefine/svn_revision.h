// 这个文件是根据SVN自动生成的版本文件
// 不需要手动修改
// 生成时间 2018-2-18 12:4:41

#ifndef SVN_REVISION_H
#define SVN_REVISION_H

static int CLIENT_BUILD_REVISION = 6737;			//Client		版本号

static int BETON_BUILD_REVISION = 6737;			//Beton			版本号

static int COOL3D_BUILD_REVISION = 6737;			//Cool3D		版本号

static int LOONGDB_BUILD_REVISION = 0;			//LoongDB		版本号

static int LOONGLOGIN_BUILD_REVISION = 0;		//LoongLogin	版本号

static int LOONGWORLD_BUILD_REVISION = 0;		//LoongWorld	版本号

static int LUA_BUILD_REVISION = 6737;				//Lua			版本号

static int SERVERDEFINE_BUILD_REVISION = 6737;	//ServerDefine	版本号

static int VENGINE_BUILD_REVISION = 6737;			//vEngine		版本号

static int WORLDBASE_BUILD_REVISION = 6737;		//WorldBase		版本号

static int WORLDDEFINE_BUILD_REVISION = 6737;		//WorldDefine	版本号

static int XMLREADER_BUILD_REVISION = 6737;		//XMLReader		版本号

#endif		 // SVN_REVISION_H

